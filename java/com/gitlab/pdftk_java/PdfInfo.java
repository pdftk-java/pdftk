/*
 *   This file is part of the pdftk port to java
 *
 *   Copyright (c) Marc Vinyals 2017-2023
 *
 *   The program is a java port of PDFtk, the PDF Toolkit
 *   Copyright (c) 2003-2013 Steward and Lee, LLC
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.pdftk_java;

// created for data import, maybe useful for export, too
class PdfInfo {
  static final String PREFIX = "Info";
  static final String BEGIN_MARK = "InfoBegin";
  static final String KEY_LABEL = "InfoKey:";
  static final String VALUE_LABEL = "InfoValue:";

  String m_key = null;
  String m_value = null;

  boolean valid() {
    return (m_key != null && m_value != null);
  }

  public String toString() {
    return BEGIN_MARK
        + System.lineSeparator()
        + KEY_LABEL
        + " "
        + m_key
        + System.lineSeparator()
        + VALUE_LABEL
        + " "
        + m_value
        + System.lineSeparator();
  }

  boolean loadKey(String buff) {
    LoadableString loader = new LoadableString(m_key);
    boolean success = loader.LoadString(buff, KEY_LABEL);
    m_key = loader.ss;
    return success;
  }

  boolean loadValue(String buff) {
    LoadableString loader = new LoadableString(m_value);
    boolean success = loader.LoadString(buff, VALUE_LABEL);
    m_value = loader.ss;
    return success;
  }
}
